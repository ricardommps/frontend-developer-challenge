## Clone project
```bash
git clone https://github.com/ricardommps/frontend-developer-challenge.git
cd frontend-developer-challenge
git checkout -b ricardo-matta
```

##  How to install
```bash
npm install
```
### Start the client
```bash
npm run dev
```
### Live Preview
https://ricard-matta-linxs.herokuapp.com/