// @flow

import React, { Component } from 'react'
import Button from '@material-ui/core/Button';

class Footer extends Component {
    render() {
        return (
        <div className="footer">
            <div className="row text-container">
                 <p>Testando suas habilidades em HTML, CSS e JS.</p>
                 <p>Linx Impulse</p>
                 <p>2019</p>
            </div>
        </div>

        )
    }
}

export default Footer